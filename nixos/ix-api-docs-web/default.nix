{config, pkgs, lib, ...}:
let
  web = with import <nixpkgs> {};
     callPackage ../pkgs/ix-api-docs-web.nix {}; 

  mkBuild = name: cfg:
    let
      buildLatest = if cfg.buildLatest then ''
        ${web}/bin/ix-api-docs-build-latest -webroot ${cfg.webroot} -docker ${cfg.docker}
      '' else "";

      buildWebroot = ''
        ${web}/bin/ix-api-docs-build-webroot -webroot ${cfg.webroot}
      '';
    in {
      wantedBy = [ "multi-user.target" ];
      after = [ "network.target" ];
      description = "build ix-api docs: ${name}";
      serviceConfig = {
        Type = "oneshot";
      };
      script = ''
        ${buildLatest}
        ${buildWebroot}
      '';
  };

  # Service for serving the static content
  mkServe = name: cfg: {
    wantedBy = [ "multi-user.target" ];
    after = [ "network.target" ];
    description = "serve docs ${name} static";
    serviceConfig = {
      ExecStart = "${web}/bin/ix-api-docs-serve -webroot ${cfg.webroot} -listen ${cfg.listen}";
    };
  };

  opts = {config, name, ...}: with lib; with types; {
    options = {
      enable = mkOption {
        type = bool;
        description = "enable ix-api docs ${name} static website";
        default = true;
      };
      webroot = mkOption {
        type = str;
        default = "/var/lib/ix-api-docs-${name}";
      };
      listen = mkOption {
        type = nullOr str;
        default = null;
      };
      buildLatest = mkOption {
        type = bool;
        default = true;
        description = "build latest version of ix-api-docs in webroot";
      };
      docker = mkOption {
        type = str;
        default = "/run/current-system/sw/bin/docker";
        description = "path to docker binary";
      };
    };
  };

  # Make services
  mkServices = attrs:
      with lib; concatMapAttrs (
        name: cfg: 
          if cfg.enable then {
            "ix-api-docs-${name}" = mkBuild name cfg;
            "ix-api-docs-${name}-serve" = mkServe name cfg;
          } else {}
      ) attrs;
in 
{
  options = {
    ix-api-docs-web = with lib; with types; mkOption {
      type = attrsOf (submodule opts);
    };
  };

  config = {
    # We need docker for this
    virtualisation.docker.enable = true;

    # Service for refreshing the webroot
    systemd.services = mkServices config.ix-api-docs-web;
  };
}

