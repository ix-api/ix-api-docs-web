{buildGoModule, nix-gitignore, ...}:

buildGoModule rec {
    pname = "ix-api-docs-web";
    version = "1.0.0";
    src = nix-gitignore.gitignoreSource [] ../..;
    vendorHash = null;
}
