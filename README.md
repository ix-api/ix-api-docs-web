
# IX-API: docs

This repository contains a generator for rendering the index page
and redocs page for all schemas found in the webroot path.

A generator for rendering the latest release using docker is also
included.

A webserver (e.g. an nginx) should be pointed to the `webroot/`
path using it as webroot serving the static content.


## Usage

All of the following commands are idempotent in the sense
that unless a new version is release, nothing should change
regarding how often they are run.

In the webroot all releases should be present containing
a schema.json, ix-api-schema-$VERSION.{json, yml}.

The index page and redoc page are created by the webroot builder.

### Generate latest schema json

The following command will generate the schema YAML and
json under `$WEBROOT/<version>/`.

    ix-api-docs-build-latest -webroot /path/to/web


### Generate webroot index

We should now refresh the index.html in the webroot
and make sure assets are installed.

    ix-api-docs-build-webroot -webroot /path/to/web


