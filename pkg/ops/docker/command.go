package docker

import (
	"flag"
	"log"
	"os"
	"os/exec"
	"strings"
)

var Docker string

func init() {
	flag.StringVar(&Docker, "docker", "docker", "path to docker command")
}

// Command is a docker command
type Command string

// CommandRun executes a docker image
var CommandRun = Command("run")

// CommandPull pulls a docker image
var CommandPull = Command("pull")

// A Volume mount point
type Volume struct {
	Host      string
	Container string
}

// Image wraps the docker image
type Image string

// Run executes a docker image
func (img Image) Run(args ...any) ([]byte, error) {
	run := append([]any{img}, args...)
	return Exec(CommandRun, run...)
}

// Pull an image
func (img Image) Pull() ([]byte, error) {
	return Exec(CommandPull, img)
}

// Exec runs a docker command
func Exec(cmd Command, args ...any) ([]byte, error) {
	log.Println("[docker]", Docker, args)

	volumes := []Volume{}
	image := Image("")
	params := []any{}

	for _, arg := range args {
		switch arg := arg.(type) {
		case Volume:
			volumes = append(volumes, arg)
		case Image:
			image = arg
		default:
			params = append(params, arg)
		}
	}

	ops := []string{string(cmd)}

	// Volumes
	for _, volume := range volumes {
		ops = append(ops, "-v", volume.Host+":"+volume.Container)
	}

	// Defaults
	if cmd == CommandRun {
		ops = append(ops, "-t", "--rm")
	}

	// Params
	ops = append(ops, string(image))
	for _, param := range params {
		s, ok := param.(string)
		if ok {
			ops = append(ops, s)
		}
	}

	engine := strings.Split(Docker, " ")
	cmdArgs := append(engine[1:], ops...)
	run := exec.Command(engine[0], cmdArgs...)
	run.Env = os.Environ()
	out, err := run.Output()
	if err != nil {
		if e, ok := err.(*exec.ExitError); ok {
			log.Println(string(e.Stderr))
		}
		return nil, err
	}
	return out, nil
}
