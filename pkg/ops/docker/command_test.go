package docker

import (
	"testing"
)

// Test Exec with an Image and Volume
func TestExec(t *testing.T) {
	Docker = "echo --"

	res, err := Exec(
		CommandRun,
		Volume{"/host", "/comtainer"},
		Image("ubuntu:latest"), "ls", "-l", "/container")

	if err != nil {
		t.Fatal(err)
	}

	t.Log("Result:")
	t.Log(string(res))

}

// Test run Image
func TestRunImage(t *testing.T) {
	Docker = "echo --"
	img := Image("ubuntu:latest")
	res, err := img.Run(
		Volume{"/host", "/comtainer"}, "ls", "-l", "/container")

	if err != nil {
		t.Fatal(err)
	}
	t.Log("Result:")
	t.Log((string(res)))

}

// Test Pull Image
func TestPullImage(t *testing.T) {
	Docker = "echo --"
	img := Image("ubuntu:latest")
	res, err := img.Pull()

	if err != nil {
		t.Fatal(err)
	}
	t.Log("Result:")
	t.Log((string(res)))

}
