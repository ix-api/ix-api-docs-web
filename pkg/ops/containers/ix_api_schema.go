package containers

import (
	"log"

	"gitlab.com/ix-api/ix-api-docs-web/pkg/ops/docker"
)

// IxApiSchemaContainer is a container imaga
type IxApiSchemaContainer string

// IxApiSchemaLatest is the schema image
var IxApiSchemaLatest IxApiSchemaContainer = "registry.gitlab.com/ix-api/ix-api-schema/live/ix-api-schema:latest"

// RenderSchena renders the schema with a format and returns
// the results as bytes.
func (c IxApiSchemaContainer) RenderSchema(format string) ([]byte, error) {
	log.Println("building schema:", format)
	return docker.Image(c).Run("generate_schema", "v2", format)
}

// ExportProblemDetails exports the problem details to a directory
// path.
func (c IxApiSchemaContainer) ExportProblemDetails(path string) error {
	log.Println("exporting problems to:", path)
	_, err := docker.Image(c).Run(
		docker.Volume{
			Host:      path,
			Container: "/out",
		},
		"export_problem_details", "v2", "/out")
	if err != nil {
		return err
	}
	return nil
}
