package main

import (
	"flag"
	"log"
	"net/http"
)

// WebrootPath contains the static files to serve
var WebrootPath string

// Listen defines the listen and serve port
var Listen string

func init() {
	flag.StringVar(
		&WebrootPath, "webroot",
		"/var/ix-api-docs-live/webroot",
		"path to the web directory with releases")
	flag.StringVar(
		&Listen, "listen",
		":8001",
		"Listen on this address")
	flag.Parse()
}

// This entire thing exists because for some reason
// I can't run a webserver in docker, publish it on a given
// port on the internal ip address and can access it from
// the loadbalancer host. Accessing it from the host running
// the container is fine.
// NO it is not the firewall. TCP communication between
// hosts on the same port is fine. However, I can not
// get the dockered thttp / nginx to answer incoming connections
// from the foreign host.
func main() {
	log.Println("starting webserver, listening on:", Listen)
	log.Println("serving:", WebrootPath)
	fs := http.FileServer(http.Dir(WebrootPath))
	http.Handle("/", fs)
	log.Fatal(http.ListenAndServe(Listen, nil))
}
