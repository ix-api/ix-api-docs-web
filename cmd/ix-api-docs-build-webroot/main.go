package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"sort"
	"strconv"
	"strings"

	"gitlab.com/ix-api/ix-api-docs-web/web/assets"
	"gitlab.com/ix-api/ix-api-docs-web/web/templates"
)

// Configuration
var (
	WebrootPath string
)

var (
	// ReMatchRelease matches a release version
	ReMatchRelease = regexp.MustCompile(`\d+\.\d+`)
)

// Versions is a sortable list of version strings
type Versions []string

// Len implements sort.Interface
func (v Versions) Len() int {
	return len(v)
}

// Less implements sort.Interface
func (v Versions) Less(i, j int) bool {
	t1 := strings.Split(v[i], ".")
	t2 := strings.Split(v[j], ".")

	I := func(s string) int {
		v, _ := strconv.Atoi(s)
		return v
	}

	v1 := make([]int, 3)
	v2 := make([]int, 3)

	for k, v := range t1 {
		v1[k] = I(v)
	}
	for k, v := range t2 {
		v2[k] = I(v)
	}

	x1 := 10e8*v1[0] + 10e4*v1[1] + v1[2] // idk
	x2 := 10e8*v2[0] + 10e4*v2[1] + v2[2]

	return x1 > x2 // inverse order
}

// Swap implements sort.Interface
func (v Versions) Swap(i, j int) {
	v[i], v[j] = v[j], v[i]
}

// Latest gets the the latest version with a fixed major
func (v Versions) Latest(major string) string {
	sort.Sort(v)
	for _, version := range v {
		if strings.HasPrefix(version, major+".") {
			return version
		}
	}
	return "unknown"
}

// releaseVersions lists all releases in the webroot
func releaseVersions(path string) (Versions, error) {
	matches, err := filepath.Glob(filepath.Join(path, "*"))
	if err != nil {
		return nil, err
	}

	v := make(Versions, 0, len(matches))
	for _, m := range matches {
		version := filepath.Base(m)
		if ReMatchRelease.MatchString(version) {
			v = append(v, version)
		}
	}

	sort.Sort(v)
	return v, nil
}

// buildVersion installs the redoc HTML
func buildVersion(version string) error {
	log.Println("building release:", version)
	if err := assets.InstallIndex(
		filepath.Join(WebrootPath, version)); err != nil {
		return err
	}
	return nil
}

// buildIndex creates the entry point with a list
// of all available versions
func buildIndex(versions Versions) error {
	return templates.WriteIndexFile(
		filepath.Join(WebrootPath, "index.html"),
		versions)
}

// buildReleasesJSON creates a releases.json file
// with an array of all versions.
func buildReleasesJSON(versions Versions) error {
	data, err := json.Marshal(versions)
	if err != nil {
		return err
	}
	if err := os.WriteFile(
		filepath.Join(WebrootPath, "releases.json"),
		data, 0644); err != nil {
		return err
	}
	return nil
}

// Symlink latest, v2 and v1
func makeSymlinks(versions Versions) error {
	// Symlink latest
	latestV2 := versions.Latest("2")
	log.Println("latest v2:", latestV2)
	os.Remove(filepath.Join(WebrootPath, "v2"))
	os.Remove(filepath.Join(WebrootPath, "latest"))
	if err := os.Symlink(
		latestV2,
		filepath.Join(WebrootPath, "v2")); err != nil {
		return err
	}
	if err := os.Symlink(
		latestV2,
		filepath.Join(WebrootPath, "latest")); err != nil {
		return err
	}

	// V1
	latestV1 := versions.Latest("1")
	log.Println("latest v1:", latestV1)
	os.Remove(filepath.Join(WebrootPath, "v1"))
	if err := os.Symlink(
		latestV1,
		filepath.Join(WebrootPath, "v1")); err != nil {
		return err
	}
	return nil
}

func init() {
	flag.StringVar(
		&WebrootPath, "webroot",
		"/opt/ix-api-docs/webroot",
		"path to the web directory with releases")
	flag.Parse()
}

func main() {
	fmt.Println("Building webroot for IX-API documentation")
	fmt.Println("WebrootPath:", WebrootPath)

	// Install assets
	if err := assets.InstallRedoc(WebrootPath); err != nil {
		log.Fatal(err)
	}

	versions, err := releaseVersions(WebrootPath)
	if err != nil {
		log.Fatal(err)
	}

	for _, v := range versions {
		if err := buildVersion(v); err != nil {
			log.Println(err)
		}
	}

	// Create index.html
	if err := buildIndex(versions); err != nil {
		log.Fatal(err)
	}

	// Create releases.json
	if err := buildReleasesJSON(versions); err != nil {
		log.Fatal(err)
	}

	// Symlink latest
	if err := makeSymlinks(versions); err != nil {
		log.Fatal(err)
	}
}
