package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"

	"gitlab.com/ix-api/ix-api-docs-web/pkg/ops/containers"
	"gitlab.com/ix-api/ix-api-docs-web/pkg/ops/docker"
)

// Configuration
var (
	WebrootPath    string
	ContainerImage string
)

// Schema is the decoded json schema
type Schema map[string]interface{}

// Version retrievs the current schema version
func (s Schema) Version() string {
	info, ok := s["info"].(map[string]interface{})
	if !ok {
		return "unknown"
	}
	version, ok := info["version"].(string)
	if !ok {
		return "unknown"
	}
	return version
}

// releasePath derives the path of the schema release
// from the version
func releasePath(version string) string {
	return filepath.Join(WebrootPath, version)
}

// assertDirectory makes sure a directory exists
func assertDirectory(path string) {
	err := os.MkdirAll(path, 0755)
	if err != nil {
		log.Fatal(err)
	}
}

// build will render the schema yml and json
// also it will export the problems
func buildDocs() {
	schemaJSON, err := containers.IxApiSchemaLatest.RenderSchema("json")
	if err != nil {
		log.Fatal(err)
	}
	schemaYAML, err := containers.IxApiSchemaLatest.RenderSchema("yaml")
	if err != nil {
		log.Fatal(err)
	}

	var schema Schema
	if err := json.Unmarshal(schemaJSON, &schema); err != nil {
		log.Fatal(err)
	}
	version := schema.Version()
	path := releasePath(version)
	log.Println("schema version:", version)

	assertDirectory(path)

	// Schema JSON and YML
	if err := os.WriteFile(
		filepath.Join(path, "schema.json"),
		schemaJSON,
		0644); err != nil {
		log.Fatal(err)
	}
	if err := os.WriteFile(
		filepath.Join(path, fmt.Sprintf("ix-api-schema-%s.json", version)),
		schemaJSON,
		0644); err != nil {
		log.Fatal(err)
	}
	if err := os.WriteFile(
		filepath.Join(path, "schema.yml"),
		schemaYAML,
		0644); err != nil {
		log.Fatal(err)
	}
	if err := os.WriteFile(
		filepath.Join(path, fmt.Sprintf("ix-api-schema-%s.yml", version)),
		schemaYAML,
		0644); err != nil {
		log.Fatal(err)
	}

	// Problem Details
	if err := containers.IxApiSchemaLatest.ExportProblemDetails(
		filepath.Join(path, "problems")); err != nil {
		log.Fatal(err)
	}

}

func init() {
	flag.StringVar(
		&WebrootPath, "webroot",
		"/opt/ix-api-docs/webroot",
		"path to the web directory with releases")
	flag.StringVar(
		&ContainerImage, "image",
		"registry.gitlab.com/ix-api/ix-api-schema/live/ix-api-schema:latest",
		"docker image generating the schema")
	flag.Parse()
}

func main() {
	fmt.Println("Rendering latest IX-API documentation")
	fmt.Println("WebrootPath:", WebrootPath)
	fmt.Println("ContainerImage:", ContainerImage)

	// Fetch container image
	_, err := docker.Image(ContainerImage).Pull()
	if err != nil {
		log.Fatal(err)
	}

	// Build docs using image
	buildDocs()
}
