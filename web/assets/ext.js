/**
 * Extension to inject logo and version
 * select into sidebar.
 *
 * @author: Annika Hannig
 */

// Wrapper for createElement to conveniently create
// nodes and set attributes.
function E(element, attrs, ...children) {
  const e = document.createElement(element);
  for (const key in attrs) {
    e.setAttribute(key, attrs[key]); 
  }
  for (let c of children) {
    if (typeof(c) === "string") {
      c = document.createTextNode(c);
    }
    e.appendChild(c);
  }
  return e;
}

function fetchReleases() {
  return fetch("/releases.json")
    .then(function(res) { return res.json() }); 
}


function Sidebar(currentRelease) {
  return E("div", {"class": "sidebar-ext"}, 
    Logo(),
    VersionSelect(currentRelease));
}

// Show a logo
function Logo() {
  return E("a", {"href": "#"},
    E("img", {
      "class": "brand-logo",
      "src": "https://ix-api.net/images/logo.svg",
    }));
}

// Create a select element with all versions
function VersionSelect(currentRelease) {
  const v1 = E("optgroup", {"label": "v1"});
  const v2 = E("optgroup", {"label": "v2"});
  const select = E("select", {"class": "version-select"}, v2, v1);

  select.addEventListener("change", function(e) {
    document.location.href = "/" + e.target.value;
  });

  const addOption = function(value) {
    let group = v2;
    if (value[0] === "1") {
      group = v1; 
    }
    let attrs = {"value": value};
    if (value === currentRelease) {
      attrs["selected"] = "selected";
    }
    group.appendChild(E("option", attrs, value));
  };

  fetchReleases().then(function(releases) {
    releases.map(addOption);
  });

  return select; 
}

// Get current release from document
function getCurrentRelease() {
  return document.location.pathname.split("/")[1];
}

function onRedocInit(redoc) {
  // Get current release
  let currentRelease = getCurrentRelease();
  let menuContainer = redoc.getElementsByClassName("menu-content")[0];
  menuContainer.prepend(Sidebar(currentRelease));
}


// Register mutation observer on the document body and invoke
// onRedocInit when redoc is available.
const observer = new MutationObserver((mutationList, observer) => {
    for (const mutation of mutationList) {
        if (mutation.target.nodeName === "REDOC") {
            observer.disconnect();
            onRedocInit(mutation.target);
            return;
        }
    }
});

observer.observe(document.body, {
    childList: true,
    subtree: true,
});

