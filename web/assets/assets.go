package assets

import (
	_ "embed" // Embedding files
	"os"
	"path/filepath"
)

// RedocJS contains the standalone redoc
//go:embed redoc.standalone.js
var RedocJS []byte

// RedocLicense includes the redoc license
//go:embed redoc.standalone.js.LICENSE.txt
var RedocLicense []byte

// RedocHTML is the HTML file serving the redoc page
//go:embed redoc.html
var RedocHTML []byte

// ExtJS is an extension script injecting a logo
// and a version select.
//go:embed ext.js
var ExtJS []byte

// InstallRedoc copies the redoc license
func InstallRedoc(webroot string) error {
	dst := filepath.Join(webroot, "assets")
	if err := os.MkdirAll(dst, 0755); err != nil {
		return err
	}
	// Write JS
	if err := os.WriteFile(
		filepath.Join(dst, "redoc.standalone.js"),
		RedocJS, 0644); err != nil {
		return err
	}
	if err := os.WriteFile(
		filepath.Join(dst, "ext.js"),
		ExtJS, 0644); err != nil {
		return err
	}
	// LICENSE
	if err := os.WriteFile(
		filepath.Join(dst, "redoc.standalone.js.LICENSE.txt"),
		RedocLicense, 0644); err != nil {
		return err
	}
	return nil
}

// InstallIndex copies the redoc index HTML to
// a target directory
func InstallIndex(dst string) error {
	if err := os.WriteFile(
		filepath.Join(dst, "index.html"),
		RedocHTML, 0644); err != nil {
		return err
	}
	return nil
}
