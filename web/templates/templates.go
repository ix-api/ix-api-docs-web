package templates

import (
	_ "embed" // Embedding
	"html/template"
	"log"
	"os"
)

//go:embed index.html
var indexHTML string

var indexTmpl *template.Template

func init() {
	t, err := template.New("index").Parse(indexHTML)
	if err != nil {
		log.Fatal(err)
	}
	indexTmpl = t
}

// WriteIndexFile renders the index template to a file
func WriteIndexFile(dst string, data interface{}) error {
	f, err := os.OpenFile(dst, os.O_RDWR|os.O_CREATE, 0644)
	if err != nil {
		return err
	}
	defer f.Close()
	return indexTmpl.Execute(f, data)
}
